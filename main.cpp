﻿#include <cstdint>
#include <cstring>
#include <iostream>

// link to this example: https://llvm.org/docs/LibFuzzer.html#more-examples
std::size_t toy_example(const char* str, std::size_t size)
{
    if (size > 0 && str[0] == 'H')
        if (size > 1 && str[1] == 'I')
            if (size > 2 && str[2] == '!')
                __builtin_trap(); // this macros causes the program to terminate abnormally.
    return 0;
}

// out-of-memory
void memory_leak_test(const char* str, const std::size_t size)
{
    char *pointer = nullptr;
    for(std::size_t i = 0; i < size; i++ )
    {
        pointer = new char[100];
    }

    delete [] pointer;
}


void buffer_oveflow_test1(const char* str, const std::size_t size)
{
    char buf[size];
    strcpy(buf, str);
}

bool buffer_oveflow_test2(const char* str, std::size_t size)
{
    return size >= 3 &&
            str[0] == 'F' &&
            str[1] == 'U' &&
            str[2] == 'Z' &&
            str[3] == 'Z';
}

extern "C" int LLVMFuzzerTestOneInput(const uint8_t *Data, std::size_t Size)
{
    toy_example(reinterpret_cast<const char*>(Data), Size);
    // buffer_oveflow_test1(reinterpret_cast<const char*>(Data), Size);
    // buffer_oveflow_test2(reinterpret_cast<const char*>(Data), Size);
    // memory_leak_test(reinterpret_cast<const char*>(Data), Size);
    return 0;
}
