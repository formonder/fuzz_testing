# Fuzz_Testing 
# Compiler:
clang-8

# flags:
 -std=c++14                                   # C++14 standart
 
 -g                                           # Generate complete debug info.
 
 -fsanitize=fuzzer                            # Builds the fuzz target w/o sanitizers
 
 -fsanitize=fuzzer,address                    # Builds the fuzz target with ASAN
 
 -fsanitize=fuzzer,signed-integer-overflow    # Builds the fuzz target with a part of UBSAN
 
 -fsanitize=fuzzer,memory                     # Builds the fuzz target with MSAN

# console commands:
clang++-8 ./main.cpp -g -O1 -std=c++14 -fsanitize=fuzzer,address

./a.out
# Фаззинг:
Техника тестирования программного обеспечения,

часто автоматическая или полуавтоматическая, заключающая в передаче

приложению на вход неправильных, неожиданных или случайных данных.

# Цели фаззинга: является попытка обнаружить нарушения 

1) Логики валидации и верификации.

2) Логики приложения в граничных случаях. 

3) Внезапные падения сервера.

4) Попытки выявить утечки памяти или утечки информации о внутреннем

устройстве системы, через необработанные сообщения об ошибках


# Дополнительные ссылки:

https://llvm.org/docs/LibFuzzer.html

https://ru.wikipedia.org/wiki/%D0%A4%D0%B0%D0%B7%D0%B7%D0%B8%D0%BD%D0%B3

https://securelist.ru/validaciya-i-verifikaciya/27213

https://habr.com/ru/post/279535

https://www.youtube.com/watch?v=k-Cv8Q3zWNQ
